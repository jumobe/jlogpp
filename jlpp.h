// -*- mode:c++;c-file-style:"linux";indent-tabs-mode:nil;c-basic-offset:2 -*-

#ifndef JLPP_H
#define JLPP_H

#include <string>
#include <mutex>
#include <memory>

namespace jlpp
{

#define inf                                                             \
  logger::get_logger()->set_log_info(__FILE__, __func__, __LINE__, jlpp::INFO)

#define inf_t                                                           \
  logger::get_logger()->set_log_info(__FILE__, __func__, __LINE__, jlpp::INFO, true)
  
#define dbg                                                             \
  logger::get_logger()->set_log_info(__FILE__, __func__, __LINE__, jlpp::DEBUG)

#define dbg_t                                                           \
  logger::get_logger()->set_log_info(__FILE__, __func__, __LINE__, jlpp::DEBUG, true)
  
#define err                                                             \
  logger::get_logger()->set_log_info(__FILE__, __func__, __LINE__, jlpp::ERROR)

#define err_t                                                           \
  logger::get_logger()->set_log_info(__FILE__, __func__, __LINE__, jlpp::ERROR, true)

#define wrn                                                             \
  logger::get_logger()->set_log_info(__FILE__, __func__, __LINE__, jlpp::WARN)

#define wrn_t                                                           \
  logger::get_logger()->set_log_info(__FILE__, __func__, __LINE__, jlpp::WARN, true)  

#define end_t                                   \
  logger::get_logger()->print_unlock()

  
  enum level
  {
    ERROR = 1,
    WARN  = 2,
    INFO  = 3,    
    DEBUG = 4  
  };  
  
  class logger
  {

  public:

    logger();
    ~logger();

    static logger *get_logger();    

    logger& set_log_info(const std::string,
                         const std::string,                      
                         const int,
                         const jlpp::level,
                         bool lock = false);

    logger& operator<< (const std::string);
    logger& operator<< (std::mutex &);

    void set_level(const std::string);
    std::mutex& print_unlock();    

  private:

    std::string pid;
    std::string out_dir;
    bool std_out;
    jlpp::level print_level;
    std::mutex print_mtx;
    
    static std::unique_ptr<logger> instance;
    static std::mutex logger_mtx;    

    inline logger& op_common(std::string);
    void print_(const std::string);
    std::string date_now(bool hm = true) const;
    std::string out_file();
    jlpp::level read_level_env();
    jlpp::level dec_level(std::string);
    
    static const std::string level_str(jlpp::level);    
  };
}

#endif
