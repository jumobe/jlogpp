
EXEC=jlpp

HEADERS=$(wildcard *.h)

OBJS=$(patsubst %.cpp, %.o, $(wildcard *.cpp))

CXX = g++

CXXFLAGS = -Wall -Wextra -std=gnu++14 -fPIC

CXXLIBS = -lpthread

# PREFIX environment variable, if it is not set, set default value
ifeq ($(PREFIX),) 
	PREFIX := /usr/local
endif


all: lib$(EXEC).so

lib$(EXEC).so: $(OBJS)
	$(CXX) -shared $^ -o $@ $(CXXLIBS)

$(EXEC): $(OBJS)
	$(CXX) $^ -o $@ $(CXXLIBS)


%.o: %.cpp
	$(CXX) $(CXXFLAGS) -c $< -o $@

clean: 
	rm -rf *.o $(EXEC) *.log *.so

install: lib$(EXEC).so
	install -d $(PREFIX)/lib
	install -m 644 lib$(EXEC).so $(PREFIX)/lib/
	install -d $(PREFIX)/include
	install -m 644 $(HEADERS) $(PREFIX)/include/

uninstall:
	rm -rf $(PREFIX)/lib/lib$(EXEC).so $(PREFIX)/include/$(HEADERS)
