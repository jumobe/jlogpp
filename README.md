# JLOG++
A simple C++ logger library.

### Environment variables

- `JLPP_LEVEL`: Log level
    - **Values** (Inclusive order): ERROR, WARN, INFO, DEBUG
    - **Default value**: DEBUG

- `JLPP_OUTPUT`: Log output file target folder. Default *./*
- `JLPP_STDOUT`: Print log in stdout. Default is *true*

### Single thread usage:
```cpp
#include <jlpp.h>
#include <iostream>

void exec(const std::string value)
{
  jlpp::inf << "value: <" + value << ">";
  jlpp::err << "value: <" + value << ">";
  jlpp::dbg << "value: <" + value << ">";
  jlpp::wrn << "value: <" + value << ">";
}

int main()
{
  int v = 2;
  exec(std::to_string(v));
  return 0;
}
```
```
[2019-02-03 12:39:43][INFO][st_example.cc][exec:6] value: <2>
[2019-02-03 12:39:43][ERROR][st_example.cc][exec:7] value: <2>
[2019-02-03 12:39:43][DEBUG][st_example.cc][exec:8] value: <2>
[2019-02-03 12:39:43][WARN][st_example.cc][exec:9] value: <2>
```

### Thread-safe usage:
```cpp
#include <jlpp.h>

#include <iostream>
#include <thread>

void exec(const std::string t)
{  
  for(int i = 0; i < 100; i++)
  {
    jlpp::inf_t << "thread: " + t << jlpp::end_t;
    jlpp::err_t << "thread: " + t << jlpp::end_t;
    jlpp::dbg_t << "thread: " + t << jlpp::end_t;
    jlpp::wrn_t << "thread: " + t << jlpp::end_t;
  }  
}

int main()
{
  std::thread t0(exec, "t0");
  std::thread t1(exec, "t1");
  std::thread t2(exec, "t2");

  t0.join();
  t1.join();
  t2.join();
  
  return 0;
}

```