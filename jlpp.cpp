// -*- mode:c++;c-file-style:"linux";indent-tabs-mode:nil;c-basic-offset:2 -*-

#include "jlpp.h"

#include <algorithm>
#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <iomanip>
#include <chrono>
#include <cstdlib>
#include <ctime>

#include <sys/types.h>
#include <unistd.h>

namespace jlpp
{

  // Objeto sigleton do logger
  std::unique_ptr<logger> logger::instance;

  // mutex que garante exclusao mutua para ambiente multithread
  std::mutex logger::logger_mtx;


  /**
   * Em um regiao de exclusao mutua, verifica a existencia de
   * uma instancia do logger, se nao existe eh criada.
   * Retorna a instancia logger.
   *
   */
  logger *logger::get_logger()
  {
    logger_mtx.lock();
    
    if(!instance)
    {
      std::unique_ptr<logger> l(new logger());
      instance = std::move(l);
    }
    
    logger_mtx.unlock();
    
    return instance.get();
  }

  /**
   * Printa a informacao de log baseado na hierarquia de nivel de logger.
   * A informacao sempre eh printada no arquivo.
   */
  void logger::print_(const std::string txt)
  {
    if( this->print_level <= this->read_level_env() )
    {
      std::ofstream outfile;
      outfile.open(this->out_file(), std::ios_base::app);
      outfile << txt;
      outfile.close();
    
      if (this->std_out)
      {
        std::cout << txt;
      }
    }
  }

  std::mutex& logger::print_unlock()
  {
    return this->print_mtx;
  }

  std::string logger::date_now(bool hm) const
  {
    const char* format = hm ? "%Y-%m-%d %X" : "%Y-%m-%d";
      
    auto now = std::chrono::system_clock::now();
    auto in_time_t = std::chrono::system_clock::to_time_t(now);

    std::stringstream ss;
    ss << std::put_time(std::localtime(&in_time_t), format);
    return ss.str();
  }

  std::string logger::out_file()
  {
    return this->out_dir + "jlpp_" + this->pid
      + "_" + this->date_now(false) + ".log";
  }

  logger::logger()
  {
    
    this->pid = std::to_string(getpid());

    this->out_dir = "./";
    if(const char* out_dir = std::getenv("JLPP_OUTPUT"))
    {        
      this->out_dir = out_dir;
      this->out_dir += "/";
    }

    this->std_out = true;
    if(const char* std_out = std::getenv("JLPP_STDOUT"))
    {        
      const std::string print_stdout = std_out;
      this->std_out = (print_stdout == "false") ? false : true;
    }
  }

  logger& logger::set_log_info(const std::string file,
                               const std::string func,                      
                               const int line,
                               const jlpp::level level,
                               bool lock)
  {
    std::stringstream ss;

    if (lock)
    {
      this->print_mtx.lock();
    }
      
    ss << "\n[" <<  this->date_now()
       << "][" << level_str(level) << "]["
       << file << "]["
       << func << ":"
       << line << "] ";

    this->print_level = level;
    this->print_(ss.str());
      
    return *this;
  }

  logger& logger::operator<< (const std::string txt)
  {
    this->print_(txt);
    return *this;
  }

  logger& logger::operator<< (std::mutex &mtx)
  {
    mtx.unlock();
    return *this;
  }

  /**
   * Decodifica qual nivel de log a string representa.
   */
  jlpp::level logger::dec_level(std::string l)
  {

    std::string l_str = l;
    std::transform(l_str.begin(),
                   l_str.end(),
                   l_str.begin(), ::toupper);
    
    jlpp::level lvl = jlpp::DEBUG;
    
    if(l_str == "ERROR")
    {
      lvl = jlpp::ERROR;
    }

    if(l_str == "WARN")
    {
      lvl = jlpp::WARN;
    }

    if(l_str == "INFO")
    {
      lvl = jlpp::INFO;
    }

    return lvl;
  }

  /**
   * Traduz o o nivel de log para a string informativa.
   */
  const std::string logger::level_str(jlpp::level l)
  {
    switch(l)
    {
    case jlpp::ERROR: return "ERROR";
    case jlpp::INFO:  return "INFO";
    case jlpp::DEBUG: return "DEBUG";
    case jlpp::WARN:  return "WARN";
    default:          return "";
    }
  }

  jlpp::level logger::read_level_env()
  {
    jlpp::level lvl = jlpp::DEBUG;
    if(const char* l = std::getenv("JLPP_LEVEL"))
    { 
      lvl = this->dec_level(l);
    }

    return lvl;
  }

  logger::~logger()
  {
  }
}


