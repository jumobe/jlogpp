// -*- mode:c++;c-file-style:"linux";indent-tabs-mode:nil;c-basic-offset:2 -*-

// g++ --std=gnu++14 examples/mt_example.cpp -o main -I${PWD} -L${PWD} -ljlpp -lpthread

#include <jlpp.h>

#include <iostream>
#include <thread>

void exec(const std::string t)
{  
  for(int i = 0; i < 100; i++)
  {
    jlpp::inf_t << "thread: " + t << jlpp::end_t;
    jlpp::err_t << "thread: " + t << jlpp::end_t;
    jlpp::dbg_t << "thread: " + t << jlpp::end_t;
    jlpp::wrn_t << "thread: " + t << jlpp::end_t;
  }  
}

int main()
{
  std::thread t0(exec, "t0");
  std::thread t1(exec, "t1");
  std::thread t2(exec, "t2");

  t0.join();
  t1.join();
  t2.join();
  
  return 0;
}
