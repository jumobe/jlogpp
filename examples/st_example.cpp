// -*- mode:c++;c-file-style:"linux";indent-tabs-mode:nil;c-basic-offset:2 -*-

// g++ --std=gnu++14 examples/st_example.cpp -o main -I${PWD} -L${PWD} -ljlpp -lpthread

#include <jlpp.h>
#include <iostream>

void exec(const std::string value)
{
  unsigned long v = 10;
  jlpp::err << "0)value: <"
            << std::to_string(v)
            << ">\n";

  long vv = 11;
  jlpp::dbg << "1)value: <"
            << std::to_string(vv)
            << ">\n";

  int v1 = 12;
  jlpp::dbg << "2)value: <"
            << std::to_string(v1)
            << ">\n";
}

int main()
{
  int v = 2;
  exec(std::to_string(v));
  return 0;
}
